function filterBy(arr, dataType) {

    const filteredArray = [];
    

    for (let i = 0; i < arr.length; i++) {
      
      if (typeof arr[i] !== dataType) {
    
        filteredArray.push(arr[i]);
      }
    }
    
   
    return filteredArray;
  }

    const arr = ['hello', 'world', 23, '23', null];
    const filteredArray = filterBy(arr, 'string');
    console.log(filteredArray); 